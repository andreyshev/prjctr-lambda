package main

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

var (
	imgBucket = "ha-img-bucket"
	pngBucket = "ha-png"
	gifBucket = "ha-gif"

	awsRegion = "eu-north-1"
)

func (c *Converter) ConvertHandler(ctx context.Context, s3Event events.S3Event) {
	for _, record := range s3Event.Records {
		record := record.S3
		object, err := c.S3.GetObject(
			&s3.GetObjectInput{
				Bucket: &imgBucket,
				Key:    &record.Object.Key,
			},
		)
		if err != nil {
			log.Println(err)
		}
		// get object body
		buf, _ := io.ReadAll(object.Body)
		rdr1 := bytes.NewBuffer(buf)
		rdr2 := bytes.NewBuffer(buf)

		// convert to png
		pngObj := mustConvertFileToType(rdr1, PNG)
		pngFileName := strings.Replace(record.Object.Key, ".jpg", ".png", 1)
		// put in png bucket
		uploader := s3manager.NewUploader(c.s3Sess)
		pngOutput, err := uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(pngBucket),
			Key:    aws.String(pngFileName),
			Body:   pngObj,
		})
		logAwsError(err)

		fmt.Println("png", pngOutput)
		// convert to gif
		gifObj := mustConvertFileToType(rdr2, GIF)
		gifFileName := strings.Replace(record.Object.Key, ".jpg", ".gif", 1)
		// put in git bucket
		gifOutput, err := uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(gifBucket),
			Key:    aws.String(gifFileName),
			Body:   gifObj,
		})
		logAwsError(err)

		fmt.Println("gif", gifOutput)
	}
}

type Converter struct {
	S3     *s3.S3
	s3Sess *session.Session
}

func logAwsError(err error) {
	log.Println(err)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
			}
		} else {
			fmt.Println(err.Error())
		}
		return
	}
}

func main() {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(awsRegion)},
	)
	if err != nil {
		log.Fatalf("failed to create AWS session, %v", err)
	}
	s3 := s3.New(sess)
	app := Converter{S3: s3, s3Sess: sess}
	lambda.Start(app.ConvertHandler)
}
