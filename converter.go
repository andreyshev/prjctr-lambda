package main

import (
	"bytes"
	"errors"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"os"

	"golang.org/x/image/bmp"
	"golang.org/x/image/tiff"
)

type FileType int

const (
	PNG FileType = iota
	JPG
	GIF
	WEBP
	BMP
	TIFF
	ERR
)

func getFileType(input string) FileType {
	switch input {
	case "jpg":
		fallthrough
	case "jpeg":
		return JPG
	case "png":
		return PNG
	case "gif":
		return GIF
	case "bmp":
		return BMP
	case "webp":
		return WEBP
	case "tiff":
		return TIFF
	default:
		return ERR
	}
}

func getFileExtension(input FileType) string {
	switch input {
	case JPG:
		return "jpg"
	case PNG:
		return "png"
	case GIF:
		return "gif"
	case BMP:
		return "bmp"
	case WEBP:
		return "webp"
	case TIFF:
		return "tiff"
	default:
		return ""
	}
}

func mustConvertFileToType(reader io.Reader, fileType FileType) *bytes.Buffer {
	// decode

	imageData, _, err := image.Decode(reader)
	if err != nil {
		logAndExit(errors.New("error decoding image"))
	}

	buf := new(bytes.Buffer)
	// encode in new type
	switch fileType {
	case JPG:
		err := jpeg.Encode(buf, imageData, nil)
		if err != nil {
			logAndExit(errors.New("error converting to jpeg"))
		}
	case PNG:
		err := png.Encode(buf, imageData)
		if err != nil {
			logAndExit(err)
		}
	case GIF:
		err := gif.Encode(buf, imageData, nil)
		if err != nil {
			logAndExit(errors.New("error converting to gif"))
		}
	case BMP:
		err := bmp.Encode(buf, imageData)
		if err != nil {
			logAndExit(errors.New("error converting to bmp"))
		}
	case TIFF:
		err := tiff.Encode(buf, imageData, nil)
		if err != nil {
			logAndExit(errors.New("error converting to tiff"))
		}
	}

	return buf
}

func logAndExit(err error) {
	fmt.Println(err.Error())
	os.Exit(1)
}
